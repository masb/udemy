import time
import random
print(time.gmtime(0))

print(time.time())

time_here = time.localtime()
print(time_here)
print("Year: ", time_here[0], time_here.tm_year)
print("Month: ", time_here[1], time_here.tm_mon)
print("Day: ", time_here[2], time_here.tm_mday)

# from time import time as my_timer

input("Press enter to start: ")

wait_time = random.randint(1, 5)
time.sleep(wait_time)
# start_time = my_timer()
start_time = time.time()

input("Press enter to stop: ")

# end_time = my_timer()
end_time = time.time()

print("Started at " + time.strftime("%X", time.localtime(start_time)))
print("Ended at " + time.strftime("%X", time.localtime(end_time)))

print("Your reaction time was {} seconds.".format(end_time-start_time))

print('')

print("time():\t\t\t", time.get_clock_info('time'))
print("perf_counter():\t", time.get_clock_info('perf_counter'))
print("monotonic():\t", time.get_clock_info('monotonic'))
print("process_time():\t", time.get_clock_info('process_time'))









