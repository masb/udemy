a = b = c = 12
print(b)

a, b = 15, 22
print("a = {}".format(a))
print("b = {}".format(b))

a, b = b, a
print("a = {}".format(a))
print("b = {}".format(b))
