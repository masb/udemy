fruit = {"lime": "a sweet lime",
         "orange": "an orange taste",
         "apple": "steve jobs",
         "grape": "i like it",
         "22": "this is number",
         "apple": "one more apple"}
print(fruit)
print(fruit["lime"])
# print(fruit[22])

fruit["pear"] = "an odd shaped apple"
print(fruit)

fruit["pear"] = "great with tequila"
print(fruit)

del fruit["grape"]
print(fruit)

# del fruit
# print(fruit)

while True:
    dict_key = input("enter key: ")
    if dict_key == "quit":
        break
    # if dict_key in fruit:
    #     description = fruit.get(dict_key)
    #     print(description)
    # else:
    #     print("wrong key")
    description = fruit.get(dict_key, "we dont have " + dict_key + " " + "12345")
    print(description)

print(fruit.items())

print("=" * 50)

for snack in fruit:
    print(fruit[snack])

print("+" * 50)

for i in range(10):
    for snack in fruit:
        print(snack + " is " + fruit[snack])
    print("-" * 50)

print("/" * 50)

# ordered_keys = list(fruit.keys())
# ordered_keys.sort()

# ordered_keys = sorted(list(fruit.keys()))

# for i in ordered_keys:
#     print(i + " - " + fruit[i])

for i in sorted(fruit.keys()):
    print(i + " - " + fruit[i])

print('')

# for val in fruit.values():
#     print(val)

for key in fruit.keys():
    print(fruit[key])

print('^' * 50)

print(fruit.keys())
print(fruit.items())
print(fruit.values())

print("z" * 50)

fruit_keys = fruit.keys()
print(fruit_keys)

fruit["tomato"] = "not nice with icecream"
print(fruit_keys)

print("-=-" * 50)

print(fruit)
print(fruit.items())
f_tuple = tuple(fruit.items())
print(f_tuple)
for snack in f_tuple:
    item, description = snack
    print(item + ' is ' + description)

print(dict(f_tuple))








fruit.clear()
print(fruit)
