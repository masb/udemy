fruit = {"lime": "a sweet lime",
         "orange": "an orange taste",
         "apple": "steve jobs",
         "grape": "i like it",
         "22": "this is number",
         "apple": "one more apple"}
print(fruit)

veg = {"cabbage": "every child's favorite",
       "sprouts": "mmm lovely",
       "spinach": "dont like it"}
print(veg)

veg.update(fruit)
print(veg)

# print(fruit.update(veg))
# print(fruit)

print('-' * 50)
nice_and_nasty = fruit.copy()
print(nice_and_nasty)
nice_and_nasty.update(veg)
print(nice_and_nasty)
