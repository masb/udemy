locations = {0: "in front of PC",
             1: "standing at the end",
             2: "at the top of hill",
             3: "inside a building",
             4: "in a valley",
             5: "in the forest"}
print(locations)

exits = {0: {"Q": 0},
         1: {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
         2: {"N": 5, "Q": 0},
         3: {"W": 1, "Q": 0},
         4: {"N": 1, "W": 2, "Q": 0},
         5: {"W": 2, "S": 1, "Q": 0}}

print(exits)

vocab = {"QUIT": "Q",
         "NORTH": "N",
         "SOUTH": "S",
         "WEST": "W",
         "EAST": "E"}


loc = 1
while True:
    # availExits = ''
    # for direction in exits[loc].keys():
    #     availIxits += direction + ', '
    # print(locations[loc])
    availExits = ", ".join(exits[loc].keys())
    print(locations[loc])

    if loc == 0:
        break
    direction = input("Available exits are " + availExits +": ").upper()
    print()
    if len(direction) > 1:
        for word in vocab:
            if word in direction:
                direction = vocab[word]

    if direction in exits[loc]:
        loc = exits[loc][direction]
    else:
        print("You cannot go in this direction")

print("SSSPLIT >>>>>>")
print(locations[0].split())
print(locations[4].split())
print(' '.join(locations[0].split()))