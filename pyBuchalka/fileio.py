jabber = open("/home/dima/Desktop/FileIO/sample.txt", 'r')

for line in jabber:
    if "jabberwock" in line.lower():
        print(line, end='')
    # !!!!!!!print(jabber)

jabber.close()

print('-' * 50)

with open("/home/dima/Desktop/FileIO/sample.txt", 'r') as jabber:
    for line in jabber:
        print(line)

print('*' * 50)

with open("/home/dima/Desktop/FileIO/sample.txt", 'r') as jabber:
    line = jabber.readline()
    while line:
        print(line, end='')
        line = jabber.readline()

print('|' * 50)

with open("/home/dima/Desktop/FileIO/sample.txt", 'r') as jabber:
    lines = jabber.readlines()
print(lines)

for line in lines:
    print(line, end='')

for line in lines[::-1]:
    print(line, end='')
