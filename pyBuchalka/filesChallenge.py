appLine = ''
with open("/home/dima/Desktop/cities.txt", 'a') as table_file:
    for j in range(2, 13):
        for i in range(1, 13):
            print("{0:>2} times {1} is {2}".format(i, j, i*j), file=table_file)
            appLine += "{0:>2} times {1} is {2}\n".format(i, j, i*j)
        print('=' * 50, file=table_file)
        appLine += "=" * 50 + "\n"
        # print(appLine, file=table_file)

print(appLine)
