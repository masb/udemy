for a in range(1, 20):
    print("a = {}".format(a))

number = "9,223,567,443,667,889"
for i in range(0, len(number)):
    print(number[i])

number = "0,223,567,443,667,889"
cleanedNumber = ""
for i in range(0, len(number)):
    if number[i] in "0123456789":
        cleanedNumber += number[i]
        print(number[i], end='')
newNumber = int(cleanedNumber)
print("The number is {}".format(newNumber))
