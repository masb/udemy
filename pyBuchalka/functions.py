def python_food():
    width = 80
    text = "spam and eggs"
    left_margin = (width - len(text)) // 2
    print(" " * left_margin, text)

def center_text(text):
    text = str(text)
    left_margin = (80 - len(text)) // 2
    print(" " * left_margin, text)

center_text("spam and eggs")
center_text("spam, spam and eggs")
center_text("spam, spam and eggs, eggs")
center_text(124)

print(python_food())

print("\n")
print("*" * 50)
print("\n")

def center_text2(*args, sep=' ', end='\n', file=None, flush=False):
    text = ""
    for arg in args:
        text += str(arg) + sep
    left_margin = (80 - len(text)) // 2
    print(" " * left_margin, text, end=end, file=file, flush=flush)

with open("/home/dima/Desktop/centered", mode='w') as centered_file:
    center_text2("spam and eggs", file=centered_file)
    center_text2("spam, spam and eggs", end='\n\n', file=centered_file)
    center_text2("spam, spam and eggs, eggs", file=centered_file)
    center_text2(124, end='\n\n', file=centered_file)
    center_text2("first", "second", 3, 4, "spam", sep=':', file=centered_file)

print("\n")
print("*" * 50)
print("\n")


def center_text3(*args, sep=' '):
    text = ""
    for arg in args:
        text += str(arg) + sep
    left_margin = (80 - len(text)) // 2
    return " " * left_margin + text

s1 = (center_text3("spam and eggs"))
print(s1)
print(center_text3("spam, spam and eggs"))
print(center_text3("spam, spam and eggs, eggs"))
print(center_text3(124))
print(center_text3("first", "second", 3, 4, "spam", sep=':'))











