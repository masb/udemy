string = "1234567890"

for char in string:
    print(char)
print("zzzz " + string[7])

my_iterator = iter(string)
print(my_iterator)
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))

for char in string:
    print(char)
print("****")
for char in iter(string):
    print(char)

myList = ["one", 2, "three", 4]
print(myList)