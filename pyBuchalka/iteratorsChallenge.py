myList = ["one", 2, "three", 4, "five", "six", 7, "eight", 9, "ten"]
myIter = iter(myList)

# print(myIter)
for n in range(len(myList)):
    print(next(myIter))
    # myIter = next(myIter)
