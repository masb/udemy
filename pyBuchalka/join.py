myList = ['a', 'b', 'c', 'd']
print(myList)

letters = 'abcdefgkjkl'

newString = ''

for c in myList:
    newString += c + ','
print(newString)

for c in myList:
    newString = ', '.join(myList)
print(newString)

for c in myList:
    newString = ' missisipi, '.join(myList)
print(newString)

print('-' * 50)

locations = {0: "in front of PC",
             1: "standing at the end",
             2: "at the top of hill",
             3: "inside a building",
             4: "in a valley",
             5: "in the forest"}
print(locations)

exits = [{"Q": 0},
         {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
         {"N": 5, "Q": 0},
         {"W": 1, "Q": 0},
         {"N": 1, "W": 2, "Q": 0},
         {"W": 2, "S": 1, "Q": 0}]
print(exits)

loc = 1
while True:
    # availExits = ''
    # for direction in exits[loc].keys():
    #     availIxits += direction + ', '
    # print(locations[loc])
    availExits = ", ".join(exits[loc].keys())
    print(locations[loc])

    if loc == 0:
        break
    direction = input("Available exits are " + availExits +": ").upper()
    print()
    if direction in exits[loc]:
        loc = exits[loc][direction]
    else:
        print("You cannot go in this direction")
