# list_1 = []
# list_2 = list()
#
# print("List 1: {}".format(list_1))
# print("List 2: {}".format(list_2))
#
# if list_1 == list_2:
#     print("lists are equal")
#
# print(list("The lists are equal"))

even = [2, 4, 6, 8]
anotherEven = list(even)
anotherEven.sort(reverse=True)
# even.sort(reverse=True)
if even == anotherEven:
    print("lists equal")
print(even)
print(anotherEven)

odd = [3, 5, 7, 9]
numbers = [even, odd]
print(numbers)

for numbersSet in numbers:
    print(numbersSet)

    for value in numbersSet:
        print(value)

menu = []
menu.append(["egg", "spam", "bacon"])
menu.append(["egg", "sausage", "bacon"])
menu.append(["egg", "spam"])
menu.append(["egg", "bacon", "spam"])
menu.append(["egg", "bacon", "sausage"])
menu.append(["spam", "bacon", "sausage", "spam"])
menu.append(["spam", "egg", "spam", "spam", "bacon", "spam"])
menu.append(["spam", "spam", "sausage", "spam"])

# print(menu[1][0])

for meal in menu:
    if not "spam" in meal:
        print(meal)
        for ingredient in meal:
            print(ingredient)
