import shelve

# bike = {"make": "honda", "model": "250 dream", "colour": "red", "engine size": 250}
# print(bike)
# print(bike["engine size"], bike["colour"])

with shelve.open('/home/dima/Desktop/bike') as bike:
    bike["make"] = "honda"
    bike["model"] = "250 dream"
    bike["colour"] = "red"
    bike["engine size"] = 250

    for key in bike:
        print(key)

    print('=' * 40)

    del bike["model"]

    for key in bike:
        print(key)

    print('=' * 40)

    print(bike["engine size"])
    print(bike["colour"])
