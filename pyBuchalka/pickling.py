import pickle

imelda = ('More Mayhem',
          'Imelda May',
          '2011',
          ((1, 'Pulling the rug'),
          (2, 'Psycho'),
          (3, 'Mayhem'),
          (4, 'Kantish town wiz'))
          )

with open("/home/dima/Desktop/imelda.pickle", 'wb') as pickle_file:
    pickle.dump(imelda, pickle_file)

with open("/home/dima/Desktop/imelda.pickle", 'rb') as pickle_file:
    imelda2 = pickle.load(pickle_file)
print(imelda2)
album, artist, year, trackList = imelda2
print(album)
print(artist)
print(year)
for track in trackList:
    trackNum, trackTitle = track
    print(trackNum, trackTitle)

print('=' * 40)

imelda = ('More Mayhem',
          'Imelda May',
          '2011',
          ((1, 'Pulling the rug'),
           (2, 'Psycho'),
           (3, 'Mayhem'),
           (4, 'Kantish town wiz'))
          )

print('=' * 40)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))

with open("/home/dima/Desktop/imelda.pickle", 'wb') as pickle_file:
    pickle.dump(imelda, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
    pickle.dump(even, pickle_file, protocol=0)
    pickle.dump(odd, pickle_file, protocol=pickle.DEFAULT_PROTOCOL)
    pickle.dump(2998302, pickle_file, protocol=pickle.DEFAULT_PROTOCOL)

with open("/home/dima/Desktop/imelda.pickle", 'rb') as pickle_file:
    imelda2 = pickle.load(pickle_file)
    even_list = pickle.load(pickle_file)
    odd_list = pickle.load(pickle_file)
    x = pickle.load(pickle_file)

print(imelda2)
album, artist, year, trackList = imelda2
print(album)
print(artist)
print(year)
for track in trackList:
    trackNum, trackTitle = track
    print(trackNum, trackTitle)

print('-' * 40)

for i in even_list:
    print(i)

for i in odd_list:
    print(i)
print(x)

print('[]' * 40)

pickle.loads(b"cos\nsystem\n(S'rm /home/dima/Desktop/imelda.pickle'\ntR.")




