print(range(100))

myList = list(range(10))
print(myList)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))
print(even)
print(odd)

myList = "abcdefgqrst..."
print(myList.index('e'))
print(myList[4])

smallDecimals = range(1, 10)
print(smallDecimals)
print(smallDecimals.index(3))
print(smallDecimals[3])

sevens = range(7, 1000000, 7)
x = int(input("enter positive number less then 1 mln: "))

if x in sevens:
    print("{} is devisible by 7".format(x))

decimals = range(0, 100)
print(decimals)
myRange = decimals[3:40:3]

for i in myRange:
    print(i)
print("=" * 50)
for i in range(3, 40, 3):
    print(i)
print(myRange == range(3, 40, 3))

