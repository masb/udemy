farm_animals = {"sheep", "cow", "hen"}
print(farm_animals)

for animal in farm_animals:
    print(animal)

print("-" * 50)

wild_animals = set(["lion", "tiger", "panther", "hare"])
print(wild_animals)

for animal in wild_animals:
    print(animal)

farm_animals.add("horse")
wild_animals.add("horse")
print(farm_animals)
print(wild_animals)

print('*' * 50)

empty_set = set()
empty_set2 = {}
empty_set.add(".")
# empty_set2.add(".")
print(empty_set)
print(empty_set2)

print('/' * 50)

even = set(range(0, 40, 2))
print(even)
print(len(even))

squares_tuple = (4, 6, 9, 16, 20)
squares = set(squares_tuple)
print(squares)
print(len(squares))

print(even.union(squares))
print(len(even.union(squares)))

print(squares.union(even))

print('e' * 50)

print(squares.intersection(squares))

print(even & squares)

print(squares.intersection(even))
print(squares & even)
print(even)
print(squares)

print('[]' * 50)

even = set(range(0, 40, 2))
print(sorted(even))

squares_tuple = (4, 6, 9, 16, 20)
squares = set(squares_tuple)
print(sorted(squares))

print("even - squares")
print(sorted(even.difference(squares)))
print(sorted(even - squares))

print("squares - even")
print(sorted(squares.difference(even)))
print(sorted(squares - even))























