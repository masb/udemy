import shelve

books = shelve.open("/home/dima/Desktop/book")
books["recipes"] = {"blt": ["bacon", "lettuce", "tomato", "bread"],
                     "beans_on_toast": ["beans", "bread"],
                     "scrambled_eggs": ["eggs", "butter", "milk"],
                     "soup": ["tin of soup"],
                     "pasta": ["pasta", "cheese"]}
books["maintance"] = {"stuck": ["oil"],
                       "loose": ["gaffer tape"]}

print(books["recipes"])

print(books["recipes"]["soup"])
print(books["recipes"]["scrambled_eggs"])
print(books["maintance"]["loose"])

books.close()