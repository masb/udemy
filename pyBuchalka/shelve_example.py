import shelve

# with shelve.open('/home/dima/Desktop/shelftest') as fruit:
fruit = shelve.open('/home/dima/Desktop/shelftest')
fruit['orange'] = 'a sweet, orange, citruis fruit'
fruit['apple'] = 'good for making...'
fruit['lemon'] = 'a sour and ...'
fruit['grape'] = 'a smlall, sweet fruit growing ...'
fruit['lime'] = 'a sour, green citrus'

print(fruit['lemon'])
print(fruit['grape'])
print(fruit['lime'])

# fruit.close()

print('=' * 40)

fruit['lime'] = 'great with tequila'

for snack in fruit:
    print(snack + ': ' + fruit[snack])

print('=' * 40)

while True:
    shelf_key = input('Please enter a fruit: ')
    if shelf_key == 'quit':
        break
    description = fruit.get(shelf_key, "We don't have a " + shelf_key)
    print(description)

print('=' * 40)

ordered_keys = list(fruit.keys())
ordered_keys.sort()

for f in ordered_keys:
    print(f + ' - ' + fruit[f])

print('=' * 40)

for v in fruit.values():
    print(v)
print(fruit.values())
print('')
for f in fruit.items():
    print(f)
print(fruit.items())

print('')

print(fruit)

fruit.close()
