import time
import datetime

print("Epoch " + time.strftime('%c', time.gmtime(0)))

print("The current timezone is {0} with offset {1}h".format(time.tzname[0], (time.timezone)/3600))

if time.daylight != 0:
    print("\tDaylight saving time is in effect for this location")
    print("\tThe DST timezone is " + time.tzname[1])

print("Local time is " + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
print("UTC time is " + time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))

print('')
print('=' * 40)
print('')

print(datetime.datetime.today())
print(datetime.datetime.now())
print(datetime.datetime.utcnow())