try:
    import tkinter
except ImportError: # python 2
    import Tkinter as tkinter

# print(tkinter.TkVersion)
# print(tkinter.TclVersion)

# kinter._test()

mainWindow = tkinter.Tk()

mainWindow.title('My GUI')
mainWindow.geometry('640x480+400+100')

label = tkinter.Label(mainWindow, text='My GUI')
label.pack(side='top')

leftFrame =tkinter.Frame(mainWindow)
leftFrame.pack(side='left', anchor='n', fill=tkinter.Y, expand=False)

canvas = tkinter.Canvas(mainWindow, relief='raised', borderwidth=1)
canvas.pack(side='left', fill=tkinter.X, expand=True)

button1 = tkinter.Button(mainWindow, text='button1')
button2 = tkinter.Button(mainWindow, text='button2')
button3 = tkinter.Button(leftFrame, text='button3')

button1.pack(side='left')
button2.pack(side='left', anchor='s')
button3.pack(side='left')


mainWindow.mainloop()