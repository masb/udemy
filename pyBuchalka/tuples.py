t = "a", "b", "c"
print(t)

print("a", "b", "c")

print(("a", "b", "c"))

welcome = "Welcome to my nightmare", "Aice Cooper", 1975
bad = "Bad company", "Bad company", 1974
budgie = "Nightfly", "Budgie", 1981
imelda = "More mayhem", "Emilda may", 2011
metallica = "One two", "Palka seljodka", 2010, (
    (1, "Pulling hug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish town waltz"))

print(welcome)
print(welcome[0])
print(welcome[1])
print(welcome[2])

# welcome[0] = "new"

budgie = budgie[0], "zopa", budgie[2]
print(budgie)

# imelda[2] = "0000"

imelda2 = ["More mayhem", "Emilda may", 2011]
imelda2[2] = "0000"
print(imelda2)

title, artist, year = imelda
print(title)
print(artist)
print(year)

print("=" * 50)

title2, artist2, year2 = imelda2
print(title2)
print(artist2)
print(year2)

print("+" * 50)

print(metallica)
title, artist, year, tracks = metallica
print(title)
print(artist)
print(year)
print(tracks)


metallica = "One two", "Palka seljodka", 2010, \
            (1, "Pulling hug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish town waltz")
title, artist, year, tracks1, tracks2, tracks3, tracks4 = metallica
print(title)
print(artist)
print(year)
print(tracks1)
print(tracks2)
print(tracks3)
print(tracks4)









