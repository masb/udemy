import random

# for i in range(1, 10):
#     print(i)

# i = 0
# while i < 22:
#     print("i is now {0:44}".format(i))
#     i += 1

availableExists = ["east", "north east", "south"]
chosenExit = ""

while chosenExit not in availableExists:
    chosenExit = input("enter direction: ")
    if chosenExit == "quit":
        print("you quit!")
        break
else:
    print("Congrats!!!")

highest = 10
answer = random.randint(1, highest)
print("Guess number from 1 to {0}: ".format(highest))
guess = int(input())
while guess != answer:
    if guess < answer:
        print("take higher")
    elif guess > answer:
        print("take lower")
    else:
        print("Gotcha!")
    guess = int(input("Try again: "))


